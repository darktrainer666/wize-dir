const { buildSchema } = require('graphql');

const schema = buildSchema(`
    type Query {
        account(id: Int!): Account
        accounts(id: Int): [Account]
    },
    type Account {
        id: Int
        name: String
        description: String
        added: String 
    }
`);

const accountData = [{
    id: 1,
    name: 'Wizeline',
    description: 'Teams inside the company working in internal projects or extracurricular activities',
    added: '2014-01-01'
}, {
    id: 2,
    name: 'Twentieth Century Fox',
    description: 'Project Fox',
    added: '2017-01-01'
}, {
    id: 3,
    name: 'Disney',
    description: 'Project Disney',
    added: '2017-02-01'
}];

module.exports = {
    schema,
    accountData,
}