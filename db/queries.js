const { accountData } = require('./schema');

const getAccount = (args) => {
    const id = args.id;
    return accountData.filter(account => account.id === id)[0] || {};
}

const listAccounts = () => {
    return accountData;
}

const root = {
    account: getAccount,
    accounts: listAccounts, 
};

module.exports = root;