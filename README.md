# WizeDir Project Backend

### Tech Stack ###
* GraphQL
* Node
* Express

### Get Started ###
`yarn install`

`npm start`

### Querying ###
Go to `/graphql` and you'll get graphical user interface to test querying.

To query via RESTful operation 

1. Make a POST Request to `/graphql`
2. Set a body field `query` with a GraphQL query.
3. Set a body field `variables` with an object and all variables for that query.

### Queries
-----------
**Get Account**

Query
```
query GetSingleAccount($accountId: Int!) {
  account(id: $accountId) {
    id
    name
    description
    added
  }
}
```
Variables
```
{
    "accountId": 2
}
```
---------
**List Account**

Query
```
query GetSingleAccount($accountId: Int!) {
  account(id: $accountId) {
    id
    name
    description
    added
  }
}
``` 
------------


