const express = require('express');
const express_graphql = require('express-graphql');
const { schema } = require('./db/schema');
const root = require('./db/queries');

const router = express.Router();

const app = express();

app.use('/graphql', express_graphql({
  schema: schema, 
  rootValue: root,
  graphiql: true,
}));

app.listen(3000, () => console.log('WizeDir server up at port 3000'));
